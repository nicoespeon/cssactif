﻿$(document).ready(function(){

		$("#generer_code").click(function(){
			/* recuperation des differentes variables */
			var typearticle = $("input[name='typearticle']:checked").val();

			var titretuto = $("#titretuto").val();
			var desctuto = $("#desctuto").val();
			var demotuto = $("#demotuto").val();
			var conttuto = $("#conttuto").val();
			var auttuto = $("#auttuto").val();
			var datetuto = $("#datetuto").val();
			var validtuto = $("#validtuto").val();
			var majtuto = $("#majtuto").val();
			
			var comptuto = "";
			var c1 = $('#comptuto_1').attr('checked');
			var c2 = $('#comptuto_2').attr('checked');
			var c3 = $('#comptuto_3').attr('checked');
			var c4 = $('#comptuto_4').attr('checked');
			var c5 = $('#comptuto_5').attr('checked');

			var verFirefox = $("#verFirefox").val();
			var verChrome = $("#verChrome").val();
			var verOpera = $("#verOpera").val();
			var verSafari = $("#verSafari").val();
			var verIE = $("#verIE").val();
			
			if (c1) { 
				comptuto += "Firefox";
				if(verFirefox) comptuto += "(ver." + verFirefox + ")"; 
			}
			if (c2) { 
				if(c1) { comptuto +=", ";}
				comptuto += "Chrome"; 
				if(verChrome) comptuto += "(ver." + verChrome + ")"; 
			}
			if (c3) {
				if (c1 || c2) { comptuto +=", ";}
				comptuto += "Safari"; 
				if(verSafari) comptuto += "(ver." + verSafari + ")"; 
			}
			if (c4) {
				if (c1 || c2 || c3) { comptuto +=", ";}
				comptuto += "Opera"; 
				if(verOpera) comptuto += "(ver." + verOpera + ")"; 
			}
			if (c5) { 
				if (c1 || c2 || c3 || c4) { comptuto +=", ";}
				comptuto += "Internet Explorer"; 
				if(verIE) comptuto += "(ver." + verIE + ")"; 
			}
			if (!c1 && !c2 && !c3 && !c4 && !c5) { comptuto = "Non renseigné"; }
			
			/* On vérifie que tous les champs sont remplis */
			if(!demotuto) demotuto = "Aucune démonstration disponible";
			if(!majtuto) majtuto = "Pas de mise à jour pour l'instant";
			if(!validtuto) validtuto = "PSEUDO STAFF";
			
			if (typearticle && titretuto && desctuto && conttuto && auttuto && datetuto) {

				/* Création du code résultant */

				if(typearticle == 'tuto') {
					var code_resultant = '<div class="cssaCommun css-tuto"><h1>' + titretuto + '</h1>\n<h2>Informations générales</h2><dl class="bloc infos-generales"><!--\n--><dt>Description :</dt><dd><!-- \n\n-->' + desctuto + '<!-- \n\n--></dd>\n<dt>Démo :</dt><dd><!-- \n\n-->' + demotuto + '<!--\n\n--></dd>\n<dt>Testé sur :</dt><dd><!-- \n\n-->' + comptuto + '<!-- \n\n--></dd>\n<dt>Crédits :</dt><dd><!-- \n\n-->Proposé par [b]' + auttuto + '[/b] le ' + datetuto + ' | Validé par [b]' + validtuto + '[/b]<!-- \n\n--></dd>\n<dt>Mise(s) à jour :</dt><dd><!-- \n\n-->' + majtuto + '<!-- \n\n--></dd></dl>\n<!--\n--><h2>Le tutoriel</h2><div class="bloc contenu-tuto">\n ' + conttuto + '\n</div></div>';
					var msg_generation = 'Vous avez généré le template de votre <strong>tutoriel</strong>, vous pouvez désormais créer un nouveau sujet dans la section <a href="http://www.css-actif.com/f13-pas-a-pas">Pas à Pas</a> ou dans la section des <a href="http://www.css-actif.com/f72-bases-css-xhtml">Bases CSS&HTML</a> et y coller le code qui suit :';
				}
				else if (typearticle == 'direxp') {
					var code_resultant = '<div class="cssaCommun direxploitable"><h1>' + titretuto + '</h1><!--\n--><div class="header"><div class="boite desc"><h2>Description</h2><div class="contenu"><!--\n\n-->' + desctuto + '<!--\n\n--></div></div><div class="boite demo"><h2>Demo</h2><div class="contenu"><!--\n\n-->' + demotuto + '<!--\n\n--></div></div></div><h2>Code</h2>[center]Vous devez poster un message à la suite pour afficher le code[/center]<div class="contenu">[hide]<!--\n\n-->' + conttuto + '<!--\n\n-->[/hide]</div><h2>Informations générales</h2><div class="infos-generales infogen"><!--\n\n--><span class="data createur">' + auttuto + '</span><!--\n\n--><span class="data datecrea">' + datetuto + '</span><!--\n\n--><span class="data compatib">' + comptuto + '</span><!--\n\n--><span class="data valid">' + validtuto + '</span><!--\n\n--><span class="data changelog">' + majtuto + '</span><!--\n\n--></div></div>';
					var msg_generation = 'Vous avez généré le template de votre <strong>Code Directement Exploitable</strong>, vous pouvez désormais créer un nouveau sujet dans la section <a href="http://www.css-actif.com/f3-codes-a-copier-coller">Codes à Copier-Coller</a> ou dans la section des <a href="http://www.css-actif.com/f10-personnalisation-fa">Personnalisations FA</a> et y coller le code qui suit :';
				}

				/* On fait apparaitre le code à copier coller */      
				$("#generateur_resultat").fadeIn(400, function(){ /*on fait apparaitre le résultat*/
					$(this).find("textarea").text(code_resultant).select(); /*on affiche le contenu et on le sélectionne dans la foulée*/
					$(this).find("p.msg_resultant").html(msg_generation);
				});
			} else {
				if(!typearticle) alert("Vous devez préciser le type d'article");
				else alert("Vous devez remplir les champs obligatoires (précédés d'un astérisque *)!");
			}
			
			return false; /*on annule le comportement par défaut du bouton de validation*/
			
			
		});	 

		
});